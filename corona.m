
# basic parameters and assumtions
population = 80000000 # population, Germany: 80 Mio.
start_infected   = 4800 # infected persons at start of simuation period (RKI, 15.03.20)
start_infected   = 40
cases = dlmread ("infections.csv", ';')

bed_intensive_care = 28000 # but 80% occupied by "daily business"

# infection rates
infection_rate1  = 1.5# how many people are infected by a single person during his illness, WHO
infection_rate2  = 4.0 # infection rate after end of shutdown, determined from growht until 16.03.20
shutdown_duration = 0 * 30 # shutdown duration in days

# illness and infection parameters
duration_illness_light = 15 # duration of the illness, light cases
duration_illness_heavy = 35 # duration of the illness, heavy cases
heavy_infection_rate = 0.15 # portion of heavy (hospitalized) cases
moratility_rate = 0.03 / heavy_infection_rate # mortality rate, only heavy cases are fatal 
immunity_rate = 0.9 # fraction of people who are immune after an infection
sim_len = floor(365 / 12 * 9 +1 ) # lengths (days) of the simulation

# data structures
infected_light = zeros( sim_len, 1 );
infected_heavy = zeros( sim_len, 1 );
healthy        = zeros( sim_len, 1 );
immune         = zeros( sim_len, 1 );
dead           = zeros( sim_len, 1 );
hospitalzed    = zeros( sim_len, 1 );
new_infected   = zeros( sim_len, 1 );

# initialization
healthy(1)          = population - start_infected;
infected_light(1)   = start_infected * (1-heavy_infection_rate);
infected_heavy(1)   = start_infected * heavy_infection_rate;
new_infected(1)     = start_infected;
immune(1)           = 0;
dead(1)             = 0;

# simulation: iterate over days
for day = [2:sim_len]

    if day < shutdown_duration
        infection_rate = infection_rate1;
    else 
        infection_rate = infection_rate2;
    endif

    # new infections
    fraction_healthy     = healthy( day - 1 ) / ( infected_heavy (day - 1) + infected_light (day - 1 ) + healthy (day - 1) + immune (day -1) );
    new_infected (day)   = infected_light (day - 1) * ( 1 + infection_rate ) / duration_illness_light * fraction_healthy + ...
                           infected_heavy (day - 1) * ( 1 + infection_rate ) / duration_illness_heavy * fraction_healthy;
    
    infected_light(day)  = infected_light(day - 1) + new_infected (day) * ( 1 - heavy_infection_rate );
    infected_heavy(day)  = infected_heavy(day - 1) + new_infected (day) * heavy_infection_rate;

    # initialization for this step
    healthy(day)         = healthy(day - 1 ) - new_infected (day);
    immune(day)          = immune(day - 1);
    dead(day)            = dead(day - 1);

    # recoveries and deaths, heavy illness
    if day > duration_illness_heavy 
        end_illness   = new_infected (day - duration_illness_heavy) * heavy_infection_rate ; # infected duration_illness ago

        # deaths
        people_dying        = end_illness * moratility_rate;
        dead(day)           = dead( day )         + people_dying;
        infected_heavy(day) = infected_heavy(day) - people_dying;

        # cures & immunity
        people_cured        = end_illness * ( 1 - moratility_rate );
        infected_heavy(day) = infected_heavy(day) - people_cured;
        immune(day)         = immune ( day  )     + people_cured * immunity_rate;
        healthy(day)        = healthy(day)        + people_cured * ( 1 - immunity_rate ); # cured, but not immune
    endif 

    # recoveries light illness -- no deaths
    if day > duration_illness_light 
        end_illness   = new_infected (day - duration_illness_light) * ( 1 - heavy_infection_rate ) ;  # infected duration_illness ago

        # cures & immunity -- no one is dying
        people_cured        = end_illness;
        infected_light(day) = infected_light (day) - people_cured;
        immune(day)         = immune ( day  )      + people_cured * immunity_rate;
        healthy(day)        = healthy(day)         + people_cured * ( 1 - immunity_rate ); # cured, but not immune
    endif 

endfor

mio = 1000000; # for y scaling
monat = 365 / 12; # for x scaling

xscale = [1:sim_len] / monat;

# clf # clear the plot area
plot (xscale,  (healthy + immune)/mio,"b-;healthy;", "linewidth", 2)
hold on
plot (xscale, immune/mio,          "b:;immune;", "linewidth", 2)
plot (xscale, infected_light/mio,  "r-;infected;", "linewidth", 2)
plot (xscale, infected_heavy/mio,  "r:;heavy;", "linewidth", 2)
plot (xscale, dead/mio,            "k-;dead;", "linewidth", 2)

# plot (cases(:,3)', "g-;real;", "linewidth", 2)

# "decoration"
axis([ 0, 80 ], [0, sim_len] / monat );
xlabel ('months');
ylabel ('person/mio');